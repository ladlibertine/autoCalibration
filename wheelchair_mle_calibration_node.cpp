// This program subscribes to the topic "scan", and logs data for calibration.

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/LaserScan.h>
#include <fstream>
#include <time.h>
#include <wheelchair_driver/WheelchairEncStamped.h>
using namespace std;

// callback function executed each time a new laser scan arrives
void laserscanMessageReceived(const sensor_msgs::LaserScan& msg){
// /*
	ros::Rate rate(1);
	std::ofstream ofs;
	ofs.open("/home/junyoung/catkin_ws/src/wheelchair_driver/src/log.log", std::ios_base::out | std::ios::app);

	ofs << "{\"timestamp\": [" << msg.header.stamp.sec << ", " << msg.header.stamp.nsec << "], ";

	// The given angle_increment does not add the total number of rays to the
	// same length as the range array. So, a new angle incremement is
	// calculated based on the length of the array.
	float adj_angle_increment = (msg.angle_max - msg.angle_min)/(msg.ranges.size()-1);
	ofs << "\"theta\": [ ";
	float angle = msg.angle_min;
	int j = 0;
	while ( j < msg.ranges.size()-1 ) {

		if ( msg.ranges[j] >= msg.range_min && msg.ranges[j] <= msg.range_max ){
			ofs << angle;
		}

		/*if ( j != msg.ranges.size()-1 && msg.ranges[j] >= msg.range_min && msg.ranges[j] <= msg.range_max){
			ofs << ", ";
		}*/
///*
		angle = angle + adj_angle_increment;
		j++;

		// if we are at the last item in array AND the range does not fall
		// within the min and max 
		if (j == msg.ranges.size()-1 && (msg.ranges[j] < msg.range_min || msg.ranges[j] > msg.range_max) ){
			// do nothing
		}
		// if we are not at the last item AND it still falls out of range
		else if (j != msg.ranges.size()-1 && (msg.ranges[j] < msg.range_min || msg.ranges[j] > msg.range_max) ){
			// do nothing
		}
		else{
			ofs << ", ";
		}

	}
	// finally, add the closing bracket and final angle if range within limits
	if ( msg.ranges[msg.ranges.size()-1] >= msg.range_min && msg.ranges[msg.ranges.size()-1] <= msg.range_max ){
		ofs << msg.angle_max << " ], ";
	}
	// omit angle if range is outside limits
	else{
		ofs << " ], ";
	}
	

	ofs << "\"readings\": [ ";
	int i = 0;
	while ( i < msg.ranges.size()-1 ){
		
		if ( msg.ranges[i] >= msg.range_min && msg.ranges[i] <= msg.range_max ){
			ofs << msg.ranges[i];
		}

		i++;
		// if we are at the last item in array AND the range does not fall
		// within the min and max 
		if (i == msg.ranges.size()-1 && (msg.ranges[i] < msg.range_min || msg.ranges[i] > msg.range_max) ){
			// do nothing
		}
		// if we are not at the last item AND it still falls out of range
		else if (i != msg.ranges.size()-1 && (msg.ranges[i] < msg.range_min || msg.ranges[i] > msg.range_max) ){
			// do nothing
		}
		else{
			ofs << ", ";
		}
		
	}
	if ( msg.ranges[msg.ranges.size()-1] >= msg.range_min && msg.ranges[msg.ranges.size()-1] <= msg.range_max ){
		ofs << msg.ranges[msg.ranges.size()-1] << " ], " << std::endl;
	}
	else{
		ofs << " ], ";
	}

//*/

}

void wheelchairencoderMessageReceived(const wheelchair_driver::WheelchairEncStamped& msg){

	ros::Rate rate(1);
	std::ofstream ofs;
	ofs.open("/home/junyoung/catkin_ws/src/wheelchair_driver/src/log.log", std::ios_base::out | std::ios::app);

	ofs << "\"left\": " << msg.tickL * 0.00024445 * 1.074 << ", ";
	ofs << "\"right\": " << msg.tickR * 0.00024524 * 1.074 <<  ", ";
	ofs << "\"leftTimestamp\": [ " << msg.header.stamp.sec << ", " << msg.header.stamp.nsec << "], ";
	ofs << "\"rightTimestamp\": [ " << msg.header.stamp.sec << ", " << msg.header.stamp.nsec << "] }" << endl;

}

int main(int argc, char **argv) {
	// Initialize the ROS system and become a node
	ros::init(argc, argv, "subsribe_to_laserscan");
	ros::NodeHandle nh;

	// Create a subscriber object
	ros::Subscriber sub = nh.subscribe("scan", 1000, &laserscanMessageReceived);
	ros::Subscriber sub2 = nh.subscribe("enc", 1000, &wheelchairencoderMessageReceived);

	// Let ROS take over
	ros::spin();
}